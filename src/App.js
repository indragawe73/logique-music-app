import React from 'react';
import { createBrowserHistory } from 'history';
import { HashRouter, Route, Routes } from 'react-router-dom';

import './App.css';
import HomePage from './pages/HomePage/HomePage';
import ListPage from './pages/ListPage/ListPage';

const history = createBrowserHistory();

const App = () => {
  return (
      <HashRouter history={history}>
        <Routes>
            <Route path="/" element={<HomePage />} />
            <Route path="/list/:listId" element={<ListPage />} />
        </Routes>
      </HashRouter>
  );
}

export default App;