import React from 'react';
import './Header.css';
import Menu from '../../assets/images/menu.png';
import Ngmusic from '../../assets/images/ngmusic.png';
import Search from '../../assets/images/search.png';
import { useNavigate } from "react-router-dom";

export default function Header (props) {
    const handleToggle = props.handleToggle;
    const navigate = useNavigate();

    const handleHome = () => {    
        navigate(`/`);
    }
    return (
        <div className='header-area'>

            <div className='header-bg'>
            </div>
            <div className='header-wrap'>
                <img onClick={handleHome} className="menu" src={Menu} alt="menu" />
                <img onClick={handleHome} className="logoNg" src={Ngmusic} alt="logoNg" />
                <img onClick={handleToggle} className="search" src={Search} alt="search" />
            </div>
        </div>
    );
};

