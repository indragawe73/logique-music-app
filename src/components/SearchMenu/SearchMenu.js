import React, { useState } from 'react';
import './SearchMenu.css'
import { useNavigate } from "react-router-dom";

const SearchMenu = (props) => {
    const popUp = props.popUp;
    const navigate = useNavigate();
    const [searchTerm, setSearchTerm] = useState('');

  const handleSubmit = (e) => {
    e.preventDefault();
    navigate(`/list/${searchTerm}`);
  };

  return (
    <form className='search-wrap' onSubmit={handleSubmit}>
        <input
            className='search-input'
            type="text"
            placeholder="Artist / Album / Title"
            value={searchTerm}
            onChange={(e) => setSearchTerm(e.target.value)}
        />
        {popUp ?
            <button className='search-btn-pop' type="submit">Search</button>
            :
            <button className='search-btn' type="submit">Search</button>
        }
    </form>
  );
};

export default SearchMenu;
