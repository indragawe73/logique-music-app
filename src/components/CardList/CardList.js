import React from 'react';
import './CardList.css';

import Play from '../../assets/images/play-circle-filled-twotone-24-px.png';
import Dollar from '../../assets/images/currency-dollar.png';

export default function CardList ({item}) {

    return (
        <div className='card-area'>
            <div className='img-area'>
                <div className='img-bg'>
                    <img src={item.artworkUrl100} alt={item.artistName} />
                </div>
                <div className='img-play'>
                    <img src={Play} alt='play' />   
                </div>
            </div>
            <div className='info-area'>
                <div className='artist'>{item.artistName}</div>
                <div className='song'>{item.trackName}</div>
                <div className='bottom-area'>
                    <div className='pop-area'>pop</div>
                    <div className='dollar-area'>
                        <img src={Dollar} alt='dolar' />    
                        <div className='dollar-text'>{item.trackPrice}</div>  
                    </div>
                    
                </div>
            </div>
        </div>
    );
};

