import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import axios from 'axios';

import Header from '../../components/Header/Header';
import CardList from '../../components/CardList/CardList';
import SearchMenu from '../../components/SearchMenu/SearchMenu';
import Close from '../../assets/images/x.png';

import './ListPage.css'

const ListPage = () => {
    const [searchTerm, setSearchTerm] = useState('');
    const [results, setResults] = useState([]);
    const [loading, setLoading] = useState(false);
    const [offset, setOffset] = useState(0);
    const [toggle, setToggle] = useState(false);
    const params = useParams();

    const search = async (val) => {
        let url;
        if (val) {
            url = `https://itunes.apple.com/search?term=${val}&limit=10&offset=${offset}`
        } else {
            url = `https://itunes.apple.com/search?term=${searchTerm}&limit=10&offset=${offset}`
        }

        try {
            setLoading(true);
            const response = await axios.get(url);
            setResults([...results, ...response.data.results]);
            setOffset((prev) => prev + 10);
        } catch (error) {
            console.error('Error searching:', error);
        } finally {
            setLoading(false);
        }
    };
    
    const handleToggle = () => {
        setToggle(!toggle)
    };

    useEffect(() => {
        setOffset(0)
        setResults([])
        setSearchTerm(params.listId)
        search(params.listId);
        setToggle(false)
    },[params.listId])

    return (
        <div className=''>
            <Header handleToggle={handleToggle} />
            <div className='wrap-list-page'>

                <div className='title-area'>
                    <div className='text-title'>Search result for :</div>
                    <div className='main-title'>{params.listId}</div>
                </div>
                <div>
                    {loading && <p>Loading...</p>}
                    <ul>
                        {results.map((item, index) => (
                            <li key={index}>
                                <CardList item={item} />
                            </li>
                        ))}
                    </ul>
                    {results.length > 0 && !loading && (
                        <div className='wrap-load-more'>
                            <button className='load-more' onClick={search}>Load More</button>
                        </div>
                    )}
                </div>
            </div>
            
            {toggle ?                
                <div className='wrap-search-pop'>
                    <img  onClick={handleToggle} className='close-btn-pop' src={Close} alt='close' />
                    <div className='search-pop-area'>
                        <p className='search-pop-title'>Search</p>
                        <SearchMenu popUp={true}/>
                    </div>

                </div>
            :null}
        </div>
    );
};

export default ListPage;
