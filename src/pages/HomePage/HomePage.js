import React from 'react';
import './HomePage.css'
import Logo from '../../assets/images/logo.png';
import SearchMenu from '../../components/SearchMenu/SearchMenu';

const HomePage = () => {

    return (
        <div className='container'>
            <div className="wrap-logo">
                <img  className="logo" src={Logo} alt="logo" />
            </div>
            <div className="wrap-search">
                <SearchMenu />
            </div>
        </div>
    );
};

export default HomePage;
